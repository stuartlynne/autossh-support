AutoSSH Reverse Tunnel                                          Stuart Lynne
Wimsey                                          Sun Nov 02 22:04:13 PST 2014 


Overview

This script sets up a reverse SSH Tunnel to an Internet accessible SSH server
allowing SSH connections to this host (which may be behind a NAT firewall).

Generating key

        ssh-keygen
        Enter file....: /home/autossh/.ssh/nopwd
        Passphrase <cr>


Contents:
    - README.txt
    - INSTALL.sh
    - bin/AUTOSSH
    - init.d/autossh-init

